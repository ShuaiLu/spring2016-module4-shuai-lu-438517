import re, os, sys,math

#define player class
class Player:
    bats =0
    hits =0
    avg =0.0  
    
    def __init__(self,name):
    	self.name = name
    
	def add(self,name,bat,hit):
		Player.bats +=bat
		Player.hits +=hit
		

#open file
if len(sys.argv) < 2:
	sys.exit("Usage: %s filename" % sys.argv[0])
 
filename = sys.argv[1]
 
if not os.path.exists(filename):
	sys.exit("Error: File '%s' not found" % sys.argv[1])

list = []

#find the player and add the record
def add(name,bat,hit):
	player =find(name)
	player.bats +=bat
	player.hits +=hit
	
	
def find(name):
	for p in list:
		if p.name == name:
			return p
	else:
		p=Player(name)
		list.append(p)
		return p
		
#read file
f = open(filename,"r")


for line in f:
	rg= '(?P<name>.*)\sbatted\s(?P<bat>\d)\stimes\swith\s(?P<hit>\d)'
	result =re.search(rg,line)
	if result:
		name =result.group('name')
		bat =int(result.group('bat'))
		hit =int(result.group('hit'))
		add(name,bat,hit)

#calculate

for p in list:
	p.avg=float(p.hits)/float(p.bats)

table =[]
	
for p in list:
    table.append([p.name, p.avg])
    
output =sorted(table, key=lambda table:table[1], reverse=True)

for item in output:
	print item[0],':',"%.3f" %round(item[1],3)

f.close()